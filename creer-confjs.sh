#!/usr/bin/env sh

if [ -f "conf.sh" ]
then
	. ./conf.sh
else
	echo "Copiez le fichier sample.conf.sh vers conf.sh et l'adapter selon vos besoins."
	exit 2
fi


if [ -f "conf.json" ]
then
	echo "Le fichier conf.json existe. Effacez le si vous êtes sûr de vouloir le recréer et recommencer."
	exit 3
else
	echo '{'>conf.json
	echo '	"sfusername":"'$SFUSERNAME'",'>>conf.json
	echo '	"sfpassword":"'$SFPASSWORD'",'>>conf.json
	echo '	"dbusername":"'$DBUSERNAME'",'>>conf.json
	echo '	"dbpassword":"'$DBPASSWORD'",'>>conf.json
	echo '	"dbscheme":"'$DBSCHEME'",'>>conf.json
	echo '	"dbhost":"'$DBHOST'",'>>conf.json
	echo '	"dbport":"'$DBPORT'",'>>conf.json
	echo '	"dbname":"'$DBNAME'"'>>conf.json
	echo '}'>>conf.json
fi
