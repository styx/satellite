#!/usr/bin/env sh

if [ -f "conf.sh" ]
then
. ./conf.sh
else
	echo "Copiez le fichier sample.conf.sh vers conf.sh et l'adapter selon vos besoins."
	exit 2
fi

if [ "$#" -ne 1 ]
then
  echo "Usage: $0 http://example.com/fil.rss"
  exit 1
fi

FEED=$1

curl -H 'Content-Type: application/json' \
            -X POST $DBSCHEME://$DBUSERNAME:$DBPASSWORD@$DBHOST:$DBPORT/$DBNAME \
            -d '{"_id": "'$FEED'","type":"fil","date":"'`date +%s`'0000"}'


# succes: {"ok":true,"id":"http://facil.qc.ca/fr/commbref/feed","rev":"1-94540028d741ad45656c579c6640134b"}
# rev va varier mais devrait commencer par "1-"

# erreur: {"error":"conflict","reason":"Document update conflict."}
