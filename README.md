# Styx Satellite
Voir <https://gitorious.org/styx/pages/Satellite>

On va utiliser les URL comme `doc._id` afin d'automatiquement éviter les doublons, stocker la date d'ajout (timestamp) dans `doc.date` et enfin la valeur `"fil"` dans `doc.type`. D'autres champs viendrons plus tard.

## Initialisation
Premièrement, copier le fichier **sample.conf.sh** dans **conf.sh** et l'éditer selon vos besoins.

```
$ cp sample.conf.sh conf.sh # attention de ne pas écraser votre config si le fichier existe déjà
$ nano conf.sh				# nano ou n'importe quel autre éditeur texte
```

Ensuite, lancer le script de création de config pour NodeJS:

```
$ ./creer-confjs.sh
```

Ceci va générer le fichier **conf.json** nécessaire au script NodeJS.

### Création de la DB

```
$ ./creer-db.sh
```

Retourne *{"ok":true}* si ça fonctionne. En cas d'erreur, vous obtiendrez quelque chose comme *{"error":"file_exists","reason":"The database could not be created, the file already exists."}*.

### Ajout des fils RSS

```
$ ./ajouter-fil.sh http://facil.qc.ca/fr/commbref/feed
```

Retourne *{"ok":true,"id":"http://facil.qc.ca/fr/commbref/feed","rev":"1-94540028d741ad45656c579c6640134b"}* en cas de succès (`rev` va varier mais devrait commencer par "1-") ou bien pour une erreur: *{"error":"conflict","reason":"Document update conflict."}*.

## Scripts Superfeedr

Vous devez maintenant démarrer le script **index.js** qui va:

* s'abonner aux nouveaux fils de la DB auprès du service Superfeedr
* recevoir les nouveaux items de Superfeedr

```
$ nodejs index.js
```

### Abonnements
La première fonctionnalité observe les nouveaux fils de notre DB et les abonne à Superfeedr.

### Nouveaux items
La seconde fonctionnalité va se connecter par XMPP à Superfeedr pour revcevoir les niveaux items des fils auxquels nous sommes abonnées et les ajouter à notre DB en tant qu'items.

**Note:** *l'API XMPP de Superfeedr fait en sorte que les items sont envoyés à un des clients XMPP connecté, choisi aléatoirement. Ça veut dire que si nos deux fonctionnalités étaient séparées en deux scripts se connectant à XMPP, l'un ou l'autre pourrait recevoir les notifications des nouveaux items, tandis que nous souhaitons les recevoir à un seul endroit - du moins pour le moment.*


### Quitter
Si vous quitter le script **index.js** (avec CTRL-C par exemple), vous risquez de manquer les notifications venant de SuperFeedr.

----

## Fils RSS:
* <http://facil.qc.ca/fr/commbref/feed>
* ...
