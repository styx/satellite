#!/usr/bin/env sh

if [ -f "conf.sh" ]
then
. ./conf.sh
else
	echo "Copiez le fichier sample.conf.sh vers conf.sh et l'adapter selon vos besoins."
	exit 2
fi

curl -X PUT $DBSCHEME://$DBUSERNAME:$DBPASSWORD@$DBHOST:$DBPORT/$DBNAME

# retourne {"ok":true} si ça fonctionne
