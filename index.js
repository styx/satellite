var conf = require("./conf.json"),
	Superfeedr = require('superfeedr'),
	nano = require('nano')(
		conf.dbscheme + '://' + conf.dbusername + ':' + conf.dbpassword +
		'@' + conf.dbhost + ':' + conf.dbport
	),
	db = nano.db.use(conf.dbname),
	client = new Superfeedr(conf.sfusername, conf.sfpassword);

// une fois connecté à SuperFeedr via XMPP
client.on('connected', function() {
	var sub = db.follow(function(err, body) {
		console.log("(feed) err: " + err);
		console.log("(feed) body: " + body);
	});
	console.log("connected!");

	// on s'intéresse aux fils auxquels nous ne sommes pas abonnés
	sub.filter = function(doc, req) {
		return !doc["abonné"] && doc.type && ("fil" === doc.type);
	}
	
	// intéressant
	sub.on("change", function(change) {
		console.log("(change)");
		console.log(JSON.stringify(change, null, " "));

		// on s'abonne au fil auprès de SuperFeedr
		client.subscribe(change.id, function(err, feed) {
			if (err) {
				console.log("sub err: " + err);
			} else {
				console.log("subscribed");
				change.doc["abonné"] = true;
				change.doc.abonnement = feed;
				// on met à jour notre document qui représente le fil
				// avec la réponse de SuperFeedr
				db.insert(change.doc, function(err, body) {
		//			console.log("(in insert) err:" + err);
					console.log(body);
				});
			}
		});
	});
//	sub.follow(); // not needed after all...

	// on reçoit un nouvel item
    client.on('notification', function(notification) {
        notification.date = Date.now();
		notification.type = "notification";
		
		// on sauve le nouvel item dans la DB
        db.insert(notification, function(err, body) {
//			console.log("(in insert) err:" + err);
			console.log(body);
		});
    });    
});
